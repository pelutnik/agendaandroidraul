/**
 * 
 */
package com.raul.AgendaAndroid;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

import android.app.Activity;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.maps.MapView;

/**
 * @author Raul
 * 
 */
public class MapActivity extends Activity {

	String direccion;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.map_activity);

		Bundle bundle = getIntent().getExtras();
		direccion = bundle.getString("direction");

		GoogleMap map = ((MapFragment) getFragmentManager().findFragmentById(
				R.id.map)).getMap();

		MapView mapView = (MapView) findViewById(R.id.map);
		mapView.setBuiltInZoomControls(true);

		List<Address> addresses = null; // mapView.getOverlays();
		// Drawable drawable =
		// this.getResources().getDrawable(R.drawable.ic_launcher);

		// MapActivity itemizedoverlay = new MapActivity(drawable, this);

		Geocoder geocoder = new Geocoder(this, Locale.ENGLISH);
		try {
			addresses = geocoder.getFromLocationName(direccion, 1);

			if (addresses.size() > 0) {
				LatLng destinationPoint = new LatLng(addresses.get(0)
						.getLatitude(), addresses.get(0).getLongitude());
				// parqueSanFranciscoMarker.remove();
				Marker direccionMaker = map.addMarker(new MarkerOptions()
						.position(destinationPoint).title(direccion));

				CameraUpdateFactory.newLatLngZoom(destinationPoint, 16);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
