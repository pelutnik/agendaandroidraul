package com.raul.AgendaAndroid.Conexion;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;

public class Connection {
	/**
	 * El buffer de entrada de datos
	 */
	private InputStream inputStream = null;
	/**
	 * La respuesta obtenida desde el servidor
	 */
	private String respuesta = "";

	/**
	 * Conecta por POST con la URL pasada como parametro, enviando los
	 * parametros contenidos en el array
	 * 
	 * @param parametros
	 *            los parametros de la peticion
	 * @param URL
	 *            la URL de conexion
	 * @throws Exception
	 */
	private void conectaPost(ArrayList<String> parametros, String URL)
			throws Exception {
		ArrayList<NameValuePair> nameValuePairs = null;
		HttpClient httpclient = null;
		HttpPost httppost = null;
		HttpResponse response = null;
		HttpEntity entity = null;
		try {
			httpclient = new DefaultHttpClient();
			httppost = new HttpPost(URL);
			nameValuePairs = new ArrayList<NameValuePair>();
			if (parametros != null) {
				for (int i = 0; i < parametros.size() - 1; i += 2) {
					nameValuePairs.add(new BasicNameValuePair(
							parametros.get(i), parametros.get(i + 1)));
				}
				httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
			}
			response = httpclient.execute(httppost);
			entity = response.getEntity();
			inputStream = entity.getContent();
		} catch (Exception e) {
			Log.e(this.getClass().getName(),
					new StringBuilder().append("Error in http connection ")
							.append(e.toString()).toString());
			throw new Exception("Error al conectar con el " + "servidor. ");
		} finally {
			// se liberan recursos
			if (entity != null) {
				entity = null;
			}
			if (response != null) {
				response = null;
			}
			if (httppost != null) {
				httppost = null;
			}
			if (httpclient != null) {
				httpclient = null;
			}
		}
	}

	/**
	 * Obtiene la respuesta del servidor
	 * 
	 * @throws Exception
	 */
	private void getRespuestaPost() throws Exception {
		BufferedReader reader = null;
		try {
			reader = new BufferedReader(new InputStreamReader(inputStream,
					"UTF-8"), 8);
			StringBuilder sb = new StringBuilder();
			String line = null;
			while ((line = reader.readLine()) != null) {
				sb.append(line).append("\n").toString();
			}
			inputStream.close();
			respuesta = sb.toString();
			Log.d(this.getClass().getName(),
					new StringBuilder().append("Cadena JSon").append(respuesta)
							.toString());
		} catch (Exception e) {
			Log.e(this.getClass().getName(),
					new StringBuilder().append("Error converting result ")
							.append(e.toString()).toString());
			throw new Exception("Error al recuperar las im�genes "
					+ "del servidor. ");
		} finally {
			if (reader != null) {
				reader.close();
			}
		}
	}

	/**
	 * Obtiene la respuesta JSON del servidor
	 * 
	 * @return la respuesta JSON del servidor
	 * @throws Exception
	 */
	@SuppressWarnings("finally")
	private JSONArray getJsonArray() throws Exception {
		JSONArray jArray = null;
		try {
			jArray = new JSONArray(respuesta);
		} catch (Exception e) {
			throw new Exception("Error al convertir a JSonArray. ");
		} finally {
			return jArray;
		}
	}

	/**
	 * Obtiene los datos desde la URL pasada con los parametros enviados por
	 * POST
	 * 
	 * @param parametros
	 *            los parametros pasados a la peticion
	 * @param URL
	 *            la URL de conexion
	 * @return los datos en formato JSON
	 * @throws Exception
	 */
	public JSONArray getServerData(ArrayList<String> parametros, String URL)
			throws Exception {
		JSONArray jsonArray = null;
		try {
			conectaPost(parametros, URL);
			getRespuestaPost();
			jsonArray = getJsonArray();
			return jsonArray;
		} catch (Exception e) {
			throw new Exception(e.getMessage());
		}
	}

	public synchronized static Bitmap getImage(String url)
			throws FileNotFoundException, Exception {
		try {
			URL aURL = new URL(url);
			URLConnection conn = aURL.openConnection();
			conn.connect();
			InputStream is = conn.getInputStream();
			BufferedInputStream bis = new BufferedInputStream(is);
			Bitmap bm = BitmapFactory.decodeStream(bis);
			bis.close();
			is.close();
			return bm;
		} catch (FileNotFoundException e) {
			throw new FileNotFoundException("No se encuentra " + "la imagen "
					+ url);
		} catch (Exception e) {
			throw new Exception("Error obteniendo la imagen " + url);
		}
	}
}