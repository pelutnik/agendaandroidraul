package com.raul.AgendaAndroid.util;

public class URLKeys {
	public static final String SERVER = "http://raulnavascues.hol.es/agenda/";
	public static final String ALL = SERVER + "all.php";
	public static final String DETAILS = SERVER + "details.php";
	public static final String INSERT = SERVER + "instert.php";
}
