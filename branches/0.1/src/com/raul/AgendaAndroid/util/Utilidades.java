package com.raul.AgendaAndroid.util;

import android.app.AlertDialog;
import android.content.Context;

public class Utilidades {

	public static void alertas(String msg, Context context) {
		// se prepara la alerta creando nueva instancia
		AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(context);
		// seleccionamos la cadena a mostrar
		dialogBuilder.setMessage(msg);
		// elegimo un titulo y configuramos para que se pueda quitar
		dialogBuilder.setCancelable(true).setTitle("Titulo de la alerta");
		// mostramos el dialogBuilder
		dialogBuilder.create().show();
	}

	/*
	 * public static void alertas(String message, Class<?> clase) { // se
	 * prepara la alerta creando nueva instancia AlertDialog.Builder
	 * dialogBuilder = new AlertDialog.Builder(clase.this); // seleccionamos la
	 * cadena a mostrar dialogBuilder.setMessage(msg); // elegimo un titulo y
	 * configuramos para que se pueda quitar
	 * dialogBuilder.setCancelable(true).setTitle("Titulo de la alerta"); //
	 * mostramos el dialogBuilder dialogBuilder.create().show(); }
	 */
}
