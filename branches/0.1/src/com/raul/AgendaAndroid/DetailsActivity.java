package com.raul.AgendaAndroid;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONObject;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.widget.TextView;

import com.raul.AgendaAndroid.Conexion.Connection;
import com.raul.AgendaAndroid.beans.Agenda;
import com.raul.AgendaAndroid.util.URLKeys;
import com.raul.AgendaAndroid.util.Utilidades;

public class DetailsActivity extends Activity {
	Agenda agenda = null;
	Integer id = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.row_contacto);

		Bundle bundle = getIntent().getExtras();
		id = Integer.parseInt(bundle.getString("id"));

		new MakePetitionTask(this).execute();
		// Mapas
		/*
		 * Button btMapa = (Button) findViewById(R.id.bt_mapa);
		 * btMapa.setOnClickListener(new View.OnClickListener() {
		 * 
		 * @Override public void onClick(View v) { String direccion =
		 * agenda.getCalle() + ", " + agenda.getLocalidad() + ", " +
		 * agenda.getProvincia(); // Llamar a la interfaz de los mapas Bundle
		 * bundle = new Bundle(); bundle.putString("direction", direccion);
		 * Intent newMap = new Intent(DetailsActivity.this, MapActivity.class);
		 * newMap.putExtras(bundle); startActivity(newMap); } });
		 */

	}

	class MakePetitionTask extends AsyncTask<Void, Void, String> {
		private ProgressDialog progressDialog;
		private final Context context;

		public MakePetitionTask(Context context) {
			this.context = context;
		}

		@Override
		protected void onPreExecute() {
			progressDialog = ProgressDialog.show(context, "",
					"Espere por favor");
		}

		@Override
		protected String doInBackground(Void... urls) {
			try {
				Connection connection = new Connection();
				ArrayList<String> parametros = new ArrayList<String>();
				parametros.add("id");
				parametros.add(new StringBuilder().append(id).toString());
				JSONArray jsonArray = connection.getServerData(parametros,
						URLKeys.DETAILS);
				if (jsonArray != null) {
					JSONObject jsonObject = jsonArray.getJSONObject(0);
					if (jsonObject != null) {
						agenda = new Agenda();
						agenda.setId(jsonObject.getInt("id"));
						agenda.setNombre(jsonObject.getString("nombre"));
						agenda.setApellido(jsonObject.getString("apellido"));
						agenda.setCalle(jsonObject.getString("calle"));
						agenda.setCp(jsonObject.getString("cp"));
						agenda.setFijo(jsonObject.getString("fijo"));
						agenda.setMovil(jsonObject.getString("movil"));
						agenda.setAnotaciones(jsonObject
								.getString("anotaciones"));
						agenda.setLocalidad(jsonObject.getString("localidad"));
						agenda.setProvincia(jsonObject.getString("provincia"));
						agenda.setEmail(jsonObject.getString("email"));
					}
				}

			} catch (Exception e) {
				Utilidades.alertas(e.getMessage(), this.context);
			}
			return "";
		}

		@Override
		protected void onPostExecute(String result) {
			this.progressDialog.cancel();
			if (agenda != null) {
				setContentView(R.layout.row_detalle);
				String nombre = agenda.getNombre() + " " + agenda.getApellido();
				String calle = agenda.getCalle();

				String anotaciones = agenda.getAnotaciones();
				String codigoPostal = agenda.getCp();
				String movil = agenda.getMovil();
				String provincia = agenda.getProvincia();
				String fijo = agenda.getFijo();
				String localidad = agenda.getLocalidad();
				String email = agenda.getEmail();

				((TextView) findViewById(R.id.tx_nombre)).setText(nombre);
				((TextView) findViewById(R.id.tx_calle)).setText(calle);
				((TextView) findViewById(R.id.tx_cp)).setText(codigoPostal);
				((TextView) findViewById(R.id.tx_anotaciones))
						.setText(anotaciones);
				((TextView) findViewById(R.id.tx_fijo)).setText(fijo);
				((TextView) findViewById(R.id.tx_localidad)).setText(localidad);
				((TextView) findViewById(R.id.tx_provincia)).setText(provincia);
				((TextView) findViewById(R.id.tx_movil)).setText(movil);
				((TextView) findViewById(R.id.tx_email)).setText(email);
			}

		}
	}

}
