package com.raul.AgendaAndroid.beans;

public class Agenda {
	private int id;
	private String nombre;
	private String apellido;
	private String calle;
	private String cp;
	private String localidad;
	private String provincia;
	private String movil;
	private String fijo;
	private String anotaciones;
	private String email;

	public Agenda() {
		super();
	}

	public int getId() {
		return id;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellido() {
		return apellido;
	}

	public void setApellido(String apellido) {
		this.apellido = apellido;
	}

	public String getCalle() {
		return calle;
	}

	public void setCalle(String calle) {
		this.calle = calle;
	}

	public String getCp() {
		return cp;
	}

	public void setCp(String cp) {
		this.cp = cp;
	}

	public String getLocalidad() {
		return localidad;
	}

	public void setLocalidad(String localidad) {
		this.localidad = localidad;
	}

	public String getProvincia() {
		return provincia;
	}

	public void setProvincia(String provincia) {
		this.provincia = provincia;
	}

	public String getMovil() {
		return movil;
	}

	public void setMovil(String movil) {
		this.movil = movil;
	}

	public String getFijo() {
		return fijo;
	}

	public void setFijo(String fijo) {
		this.fijo = fijo;
	}

	public String getAnotaciones() {
		return anotaciones;
	}

	public void setAnotaciones(String anotaciones) {
		this.anotaciones = anotaciones;
	}

	@Override
	public String toString() {
		return "Agenda [id=" + id + ", nombre=" + nombre + ", apellido="
				+ apellido + ", calle=" + calle + ", cp=" + cp + ", localidad="
				+ localidad + ", provincia=" + provincia + ", movil=" + movil
				+ ", fijo=" + fijo + ", anotaciones=" + anotaciones
				+ ", email=" + email + "]";
	}

}
